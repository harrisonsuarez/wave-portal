async function main() {
  const [deployer] = await ethers.getSigners();

  console.log('Deploying contracts with the account:', deployer.address);
  console.log('Account balance:', (await deployer.getBalance()).toString());

  const Token = await ethers.getContractFactory('WavePortal');
  const token = await Token.deploy({
    value: ethers.utils.parseEther('0.1'),
  });

  console.log('WavePortal address:', token.address);
}

main()
  .then(() => process.exit(0))
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
