// NOTE: The hardhat package provides ethers in the global namespace. Or,
// if you prefer to be explicit, you can import it using:
// const { ethers } = require('hardhat');
async function main() {
  const [owner, randomPerson] = await ethers.getSigners();
  // Compile and generate the necessary files
  // into the /artifacts directory
  const waveContractFactory = await hre.ethers.getContractFactory('WavePortal');

  // Instantiate a local Ethereum network for this contract only
  const WaveContract = await waveContractFactory.deploy({
    value: hre.ethers.utils.parseEther('0.1'),
  });

  // Await for the contract to be deployed, at which point the
  // constructor will be invoked
  await WaveContract.deployed();

  console.log('WaveContract deployed to: ', WaveContract.address);
  console.log('WaveContract deployed by: ', owner.address);

  let balance = await hre.ethers.provider.getBalance(WaveContract.address);
  console.log('WaveContract balance: ', hre.ethers.utils.formatEther(balance));

  let waveCount = await WaveContract.getTotalWaves();

  let waveTxn = await WaveContract.wave();
  await waveTxn.wait();

  await WaveContract.post('Jack was here');
  await WaveContract.post('Jill was here');
  await WaveContract.post('Joanne was here');

  const posts = await WaveContract.getPosts();

  console.log(posts.map((p) => p.message));

  waveCount = await WaveContract.getTotalWaves();

  waveTxn = await WaveContract.connect(randomPerson).wave();
  await waveTxn.wait();

  waveCount = await WaveContract.getTotalWaves();

  const post = await WaveContract.getPostById(1);

  waveTxn = await WaveContract.connect(randomPerson).wave();
  await waveTxn.wait();

  balance = await hre.ethers.provider.getBalance(WaveContract.address);
  console.log('WaveContract balance: ', hre.ethers.utils.formatEther(balance));
}

main()
  .then(() => process.exit(0))
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
