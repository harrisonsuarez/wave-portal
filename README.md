# WavePortal
This Smart Contract is a Solidity proof-of-concept that introduces blockchain-based patterns using the Ethereum network.

## Visit
You can interact with and preview this contract at:

[https://waveportal-baseline-student.harrisonsuarez.repl.co/](https://waveportal-baseline-student.harrisonsuarez.repl.co/).

![A picture of the WavePortal user interface.](/210914 waveportal final.png "Cover Image")

**NOTE:** The contract is deployed on the *Rinkeby Test Network*. If you visit the preview, you'll only be able to interact with it if your wallet is set to interact on the Rinkeby test network.

## Summary
The contract exposes methods that allow callers to wave and post messages.

The contract also exposes methods that allow callers to:
- get total number of waves
- get a list of posts
- get a single post by id

When callers `wave` at the contract, they have a chance of receiving a .0001 ether prize.

It was heavily influenced by the [_buildspace](https://buildspace.so) *Solidity x Ethereum Smart Contracts* course.

## Features
`makeRandomNumber()` generates a pseudo random number using a combination of the block difficulty, block timestamp, and a random seed.

`struct Post` demonstrates the use of a struct.

`WaveSent` demonstates the use of an event emitter to asynchronously notify interested parties using the `contract.on('WaveSent', () => {})` syntax.

Includes a `mapping` between users who have posted and their last time of post in order to prevent spammers.

## Other
Also makes use of...

- Hardhat
- Ethers
