// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

import 'hardhat/console.sol';

contract WavePortal {
  uint totalWaves;
  uint private nextPostId;
  uint[] private postIds;
  uint private seed;

  // This maps an address to an integer, in this case the time
  // the person last waved
  mapping(address => uint) public lastWavedAt;

  constructor() payable {
    console.log('The WavePortal message board is now active!');
  }

  event WaveSent(
    uint id,
    address indexed sender,
    string message,
    uint timestamp
  );

  struct Post {
    uint id;
    address sender;
    string message;
    uint timestamp;
  }

  Post[] posts;

  function makeRandomNumber() private returns (uint) {
    uint randomNumber = (block.difficulty + block.timestamp + seed) % 100;
    console.log ('Random # generated %s', randomNumber);

    seed = randomNumber;

    return randomNumber;
  }

  function wave() public {
    require(lastWavedAt[msg.sender] + 15 minutes < block.timestamp, 'You must wait fifteen minutes between waves.');

    // Update the current timestamp for the sender
    lastWavedAt[msg.sender] = block.timestamp;

    totalWaves += 1;
    // msg.sender is the wallet address of the person who calls wave()
    console.log('%s waved!', msg.sender);

    uint randomNumber = makeRandomNumber();

    if (randomNumber < 50) {
      uint prizeAmount = 0.0001 ether;
      // require checks that this statement is true, and if not it quits the
      // contract. Note that address(this).balance is the balance of the
      // contract itself
      require(prizeAmount <= address(this).balance, 'Trying to withdraw more money than the contract has.');
      (bool success,) = (msg.sender).call{value: prizeAmount}('');
      require(success, 'Failed to withdraw money from contract.');
      console.log('%s won!', msg.sender);
    }
  }

  // Public and External functions have similar properties. The key difference
  // seems to be cost of memory allocation. In public functions, Solidity
  // immediately copies array arguments to memory, while external functions can
  // read directly from calldata. Memory allocation is expensive, whereas
  // reading from calldata is cheap.
  function post(string memory _message) external {
    posts.push(
      // Note that the syntax of a newly-created struct isn't like you would
      // expect in JavaScript objects. Rather, you pass parameters in the same
      // order in which they were declared.
      Post(nextPostId, msg.sender, _message, block.timestamp)
    );

    emit WaveSent(nextPostId, msg.sender, _message, block.timestamp);

    nextPostId += 1;
  }

  function getTotalWaves() view public returns (uint) {
    console.log('We have had %d total waves', totalWaves);
    return totalWaves;
  }

  function getPosts() view public returns (Post[] memory) {
    return posts;
  }

  function getPostById(uint id) view public returns (
    address,
    string memory,
    uint
  ) {
    Post memory p = posts[id];

    console.log('Message from %s at %s: %s', p.sender, p.timestamp, p.message);

    return (
      p.sender,
      p.message,
      p.timestamp
    );
  }
}